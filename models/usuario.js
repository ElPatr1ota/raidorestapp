exports = module.exports = function(app, mongoose) {

    var usuarioSchema = new mongoose.Schema({
        mail: 		{ type: String },
        pass: 		{ type: String },
        address:    { type: String },
        city:       { type: String },
        country: 	{ type: String },
        birthday:  	{ type: Date },
        genre: 		{
            type: String,
            enum: ['Femenino', 'Masculino']
        },
        typeUser: 		{
            type: String,
            enum: ['Coach', 'Suscriptor']
        }
    });

    mongoose.model('Usuario', usuarioSchema);

};