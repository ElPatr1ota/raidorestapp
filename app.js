var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    mongoose        = require('mongoose');

// Connection to DB
mongoose.connect('mongodb://localhost/usuarios', function(err, res) {
  if(err) throw err;
  console.log('Connected to Database');
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Import Models and controllers
var models     = require('./models/usuario')(app, mongoose);
var UsuarioCtrl = require('./controllers/usuarios');

// Example Route
var router = express.Router();
router.get('/', function(req, res) {
  res.send("Hello to RaidoApp!");
});
app.use(router);

// API routes
var usuarios = express.Router();

usuarios.route('/usuarios')
    .get(UsuarioCtrl.findAllUsuarios)
    .post(UsuarioCtrl.addUsuario);

usuarios.route('/usuarios/:id')
    .get(UsuarioCtrl.findById)
    .put(UsuarioCtrl.updateUsuario)
    .delete(UsuarioCtrl.deleteUsuario);

app.use(usuarios);

// Start server
app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});