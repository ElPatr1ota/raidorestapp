/**
 * Created by Andrés on 01/09/2017.
 */
//File: controllers/usuario.js
var mongoose = require('mongoose');
var Usuario  = mongoose.model('Usuario');

//GET - Return all usuarios in the DB
exports.findAllUsuarios = function(req, res) {
    Usuario.find(function(err, usuarios) {
        if(err) res.send(500, err.message);

        console.log('GET /usuario')
        res.status(200).jsonp(usuarios);
    });
};

//GET - Return a Usuario with specified ID
exports.findById = function(req, res) {
    Usuario.findById(req.params.id, function(err, usuario) {
        if(err) return res.send(500, err.message);

        console.log('GET /usuario/' + req.params.id);
        res.status(200).jsonp(usuario);
    });
};

//POST - Insert a new Usuario in the DB
exports.addUsuario = function(req, res) {
    console.log('POST');
    console.log(req.body);

    var usuario = new Usuario({
        mail:    req.body.mail,
        pass:   req.body.pass,
        address: 	  req.body.address,
        city:  req.body.city,
        country:   req.body.country,
        birthday:  req.body.birthday,
        genre:    req.body.genre,
        typeUser:  req.body.typeUser
    });

    usuario.save(function(err, usuario) {
        if(err) return res.send(500, err.message);
        res.status(200).jsonp(usuario);
    });
};

//PUT - Update a register already exists
exports.updateUsuario = function(req, res) {
    Usuario.findById(req.params.id, function(err, usuario) {
        usuario.mail   = req.body.mail;
        usuario.pass   = req.body.pass;
        usuario.city  = req.body.city;
        usuario.address    = req.body.address;
        usuario.country = req.body.country;
        usuario.birthday  = req.body.birthday;
        usuario.genre   = req.body.genre;
        usuario.typeUser = req.body.typeUser;

        usuario.save(function(err) {
            if(err) return res.send(500, err.message);
            res.status(200).jsonp(usuario);
        });
    });
};

//DELETE - Delete a Usuario with specified ID
exports.deleteUsuario = function(req, res) {
    Usuario.findById(req.params.id, function(err, usuario) {
        usuario.remove(function(err) {
            if(err) return res.send(500, err.message);
            res.status(200);
        })
    });
};